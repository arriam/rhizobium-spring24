#!/usr/bin/env julia

using Pkg
Pkg.activate(dirname(@__DIR__), io=devnull)

using FastaIO
using ArgParse
using Logging
using ProgressMeter

include("../src/MetaFASTA.jl")
using .MetaFASTA

macro no_log(ex)
    quote
        with_logger(Logging.NullLogger()) do
            $(esc(ex))
        end
    end
end

function parse_commandline()
    s = ArgParseSettings()
    s.description = "Concatenate all the sequences in single fasta file. \
    All the headers are concatenated in order of their respective seuqences \
    and prefixed with \"#<sequence_length>#\""
    s.usage = "./scripts/04_fasta_cat.jl -s data/assembly/PRJNA1049504/*"
    s.version = "1.1"
    s.add_version = true

    @add_arg_table! s begin
        "input_file"
            help = "Input file"
            required = true
            arg_type = String
            nargs = '+'

        "-o", "--output_dir"
            help = "Output dirname w.r. to directory of `input_file`"
            arg_type = String
            default = "cats"

        "-s", "--sort"
            help = "Sort sequences by length in descending order before concatenation"
            action = :store_true
    end
    return parse_args(s)
end

function cat_fasta(fasta_in, subdir_name, to_sort)
    dir_in, fastabasename = splitdir(fasta_in)
    fastaname, ext = match(MetaFASTA.FASTA_SUFFIX_GZ, fastabasename).captures
    dir_out = mkpath(joinpath(dir_in, subdir_name))
    fasta_out = joinpath(dir_out, fastaname * "_cat" * ext)

    headers = String[]
    sequences = String[]
    for (header, seq) in FastaReader(fasta_in)
        push!(headers, header)
        push!(sequences, seq)
    end
    if to_sort
        perm = sortperm(sequences; rev=true, by=length)
        permute!(sequences, perm)
        permute!(headers, perm)
    end
    seq_lens = length.(sequences)

    single_header = join([string("#", join(h, "#")) for h in zip(seq_lens, headers)])
    FastaWriter(fasta_out) do fw
        @no_log writeentry(fw, single_header, join(sequences))
    end
end

function main()
    clargs = parse_commandline()
    filenames = clargs["input_file"]
    out_dir = clargs["output_dir"]
    to_sort = clargs["sort"]

    fasta_suffix = MetaFASTA.FASTA_SUFFIX_GZ
    valid_fastanames = filter(x -> occursin(fasta_suffix, x), filenames)
    if isempty(valid_fastanames)
        @error "Only .fna .fsa .fa .fasta [.gz] file extensions are parsed."
        exit()
    end

    @showprogress "Concatenating sequences..." for file in valid_fastanames
        cat_fasta(file, out_dir, to_sort)
    end

    println("$(length(valid_fastanames)) files processed.")
end

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end