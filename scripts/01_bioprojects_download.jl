#!/usr/bin/env julia

using Pkg
Pkg.activate(dirname(@__DIR__), io=devnull)

using ArgParse
using ZipFile

function parse_commandline()
    s = ArgParseSettings()
    s.usage = "./scripts/01_bioprojects_download.jl --id PRJNA1049504 GCF_004306555.1 --source genbank -d data/assembly"
    s.description = "Fetch NCBI assemblies"
    s.epilog = "Each ID creates separate subdirectory"
    s.version = "1.3"
    s.add_version = true

    @add_arg_table! s begin
        "--id"
            help = "Bioproject ID(s)"
            required = true
            arg_type = String
            nargs = '+'
        "-d", "--output_dir"
            help = "Output directory"
            default = "."
            arg_type = String
        "-s", "--source"
            help = "Bioproject assembly source: 'RefSeq', 'GenBank' or 'all' for both. \
            Affects only `PRJ` IDs"
            default = "GenBank"
            arg_type = String
        "-u", "--uncompress"
            help = "Write fastas as uncompressed files (`.gz`-archives otherwise)"
            action = :store_true
    end
    return parse_args(s)
end

function check_env()
    infomsg = "Try recreating Conda environment from yaml file somewhere around in this repo:\n\
    conda env create -f environment.yml\n\
    conda activate rhizobium-spring24"
    errormsg = "You need to run this script inside environment with `ncbi-datasets-cli` package installed!"

    if Sys.which("datasets") === nothing
        @info infomsg
        @error errormsg
        exit()
    end
end

function unzip_with_suffix(archive_name, suffix, output_dir; compress_back=true)
    archive = ZipFile.Reader(archive_name)
    try
        for file in archive.files
            if endswith(file.name, suffix)
                filename = basename(file.name)
                if compress_back
                    gz_filename = filename * ".gz"
                    gzopen(joinpath(output_dir, gz_filename), "w") do io
                        write(io, read(file))
                    end
                else
                    write(joinpath(output_dir, filename), read(file))
                end
            end
        end
    catch e
        @error "Unexpected $e happend during unzipping $(basename(output_dir)): "
    finally
        close(archive)
    end
end

function download_assemblies(bioproject_id, temp_filename, source="GenBank")
    excode = 1
    try
        run(
        # This command requires NCBI `ncbi-datasets-cli` tool availible in working environment
        `datasets download genome accession --assembly-source $source --no-progressbar --filename $temp_filename $bioproject_id`
        )
        fsize = filesize(temp_filename) |> Base.format_bytes
        println("Downloaded $bioproject_id: $fsize")
        excode = 0
    catch ex
        @error ex
    finally
        return excode
    end
end

function fetch_genome_data(bioproject_id, output_dir; source="GenBank", compress_back=true)
    @assert lowercase(source) in ["refseq", "genbank",  "all"] "Incorrect NCBI source $source"
    
    genome_dir = joinpath(output_dir, bioproject_id)
    mkpath(genome_dir)
    if !isempty(readdir(genome_dir))
        @warn "Directory for $bioproject_id is not empty! Skipping..."
        return 0
    end

    temp_archive = tempname(genome_dir)
    println("Downloading $bioproject_id...")

    if download_assemblies(bioproject_id, temp_archive, source) != 0
        @warn "Couldn't download $(bioproject_id)"
        rm(genome_dir)
        return 0
    end
    
    suffixes = [".fasta", ".fna", ".fsa", ".fa"]
    println("$(basename(genome_dir)): extracting all $(suffixes) files...")
    for suffix in suffixes
        unzip_with_suffix(temp_archive, suffix, genome_dir; compress_back=compress_back)
    end
    println("Extracted to $genome_dir")
    return 1
end


function main()
    check_env()
    clargs = parse_commandline()
    source_db = clargs["source"]
    ids = clargs["id"]
    uncompress = clargs["uncompress"]
    output_dir = clargs["output_dir"]


    n_downloads = Threads.Atomic{Int}(0) # multithreaded counter

    println("Fetching assembly records from NCBI: $(source_db)")

    correct_PRJ = [id for id in ids if startswith(id, "PRJ")]
    correct_GCA = [id for id in ids if startswith(id, "GCA_")]
    correct_GCF = [id for id in ids if startswith(id, "GCF_")]
    correct_ids = vcat(correct_PRJ, correct_GCA, correct_GCF)

    if length(ids) != length(correct_ids)
        @warn "Some IDs appear to be incorrect and hence skipped"
    end
    compress_back = !uncompress
    # Async downloads even on single thread
    @sync for id in correct_ids        
        # Seems like overkill for typical number of bioprojects (<10), but still
        Threads.@spawn begin
            res = 0
            if startswith(id, "PRJ")
                res = fetch_genome_data(id, output_dir; source=source_db, compress_back=compress_back)
            elseif startswith(id, "GCA_")
                res = fetch_genome_data(id, output_dir; source="GenBank", compress_back=compress_back)
            elseif startswith(id, "GCF_")
                res = fetch_genome_data(id, output_dir; source="RefSeq", compress_back=compress_back)
            end
            Threads.atomic_add!(n_downloads, res)
        end
    end

    # All the hassle just to print this info msg correctly :)
    println("$(n_downloads.value) bioproject(s) were successfully downloaded and extracted.")
end


if abspath(PROGRAM_FILE) == @__FILE__
    main()
end
