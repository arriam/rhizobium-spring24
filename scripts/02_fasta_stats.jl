#!/usr/bin/env julia

using Pkg
Pkg.activate(dirname(@__DIR__), io=devnull)

using ArgParse
using UnicodePlots

include("../src/MetaFASTA.jl")
using .MetaFASTA

function parse_commandline()
    s = ArgParseSettings()
    s.usage = "./scripts/02_fasta_stats.jl data/assembly/PRJNA1049504 -b -s"
    s.description = "Basic stats about FastA files in chosen dir"
    s.version = "1.3"
    s.add_version = true
    
    @add_arg_table! s begin
        "directory"
            help = "input directory"
            default = "."
            arg_type = String

        # "-o", "--output_tsv"
        #     help = "filename to save output as tsv to"
        #     arg_type = String

        "-b", "--barplot"
            help = "Draw barplot with strain lengths"
            action = :store_true

        "-s", "--print_sorted_strains"
            help = "Print lengths of sequences, related to single strain in descending order"
            action = :store_true
    end
    return parse_args(s)
end


function count_print(cm::Dict; offset="")
    padlength = maximum(length, keys(cm)) + 1
    sorted_entries = sort(collect(cm), by=x->x[2], rev=true)
    
    for (key, value) in sorted_entries
        println(offset, rpad("$(key):", padlength, " "), rpad(" $value", 4, " "))
    end
end


function short_report(path, dir_is_repacked, dir_sequences, dir_fasta_files)    
    if dir_is_repacked
        println("Directory (repacked) $(abspath(path)):")  
    else  
        println("Directory $(abspath(path)):")
    end

    all_seqs_len = length.(dir_sequences)

    println("Total files: $(length(dir_fasta_files))")
    println("Total sequences: $(length(dir_sequences))")
    println("Total sequence length: $(bioseq_readlen(sum(all_seqs_len)))")
end

function summ_strains(dir_metas, dir_sequences, dir_stripped_filenames, dir_is_repacked)
    all_seqs_len = length.(dir_sequences)

    strains = dir_is_repacked ?
        dir_stripped_filenames :
        Vector{String}(getfield.(
            getfield.(
                dir_metas,
                :header
            ), :strain
        ))

    strain_sums = Dict(
        strain => sum(all_seqs_len[strains .== strain]) for strain in unique(strains)
    )

    strain_names = rpad.(collect(keys(strain_sums)), 6, " ")
    strain_lengths = collect(values(strain_sums))
    perm = sortperm(strain_lengths, rev=true)
    return strain_names[perm], strain_lengths[perm], strains
end

function print_strains_sorted(strains, dir_metas)
    # Create a dictionary to store lengths for each strain
    strain_lengths = Dict{String, Vector{Int}}()

    # Populate the dictionary with lengths
    for i in eachindex(strains)
        strain = strains[i]
        len = dir_metas[i].len
        if haskey(strain_lengths, strain)
            push!(strain_lengths[strain], len)
        else
            strain_lengths[strain] = [len]
        end
    end

    # Print the header
    println("Strain\tLength")

    # Iterate over each strain
    for strain in unique(strains)
        # Sort the lengths in descending order
        sorted_lengths = sort(strain_lengths[strain], rev=true)
        print(strain)
        # Print the strain and its lengths
        for (i, len) in enumerate(sorted_lengths)
            if i == 1
                println("\t$(bioseq_readlen(len))\t<-chromosome")
            elseif i == length(sorted_lengths)
                println("└──\t$(bioseq_readlen(len))")
            else
                println("├──\t$(bioseq_readlen(len))")
            end
        end
    end
end


function main()
    clargs = parse_commandline()
    path = clargs["directory"]
    draw_barplot = clargs["barplot"]
    strains_sorted = clargs["print_sorted_strains"]

    # Necessary calculations
    dir_is_repacked = isrepacked(path)
    dir_fasta_files, dir_metas, dir_sequences, dir_stripped_filenames = scrape_dir(path; repacked_dir=dir_is_repacked)
    if isempty(dir_fasta_files)
        @info "No fasta files in $(abspath(path))"
        exit()
    end

    strain_names, strain_lengths, strains = summ_strains(dir_metas, dir_sequences, dir_stripped_filenames, dir_is_repacked)

    # Mandatory report
    short_report(path, dir_is_repacked, dir_sequences, dir_fasta_files)
    
    # -b, --barplot
    if draw_barplot
        screenwidth = min(120, floor(Int, 0.9*displaysize(stdout)[2]))
        barplot(
            strain_names,
            strain_lengths,
            title="Strain lengths",
            width=screenwidth
        ) |> println
    end

    # -s, --print_sorted_strains
    strains_sorted && print_strains_sorted(strains, dir_metas)
end


if abspath(PROGRAM_FILE) == @__FILE__
    main()
end


