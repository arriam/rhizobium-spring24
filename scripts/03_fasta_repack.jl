#!/usr/bin/env julia

using Pkg
Pkg.activate(dirname(@__DIR__), io=devnull)

using FastaIO
using ArgParse
using ProgressMeter
using REPL
using REPL.TerminalMenus

include("../src/MetaFASTA.jl")
using .MetaFASTA

function cls()
    REPL.Terminals.clear(REPL.Terminals.TTYTerminal("xterm", stdin, stdout, stderr))
end

function parse_commandline()
    s = ArgParseSettings()
    s.description = "Repack all fasta files in chosen directory based on their strains to simplify filenames and sequence headers"
    s.epilog = "Automatic mode only works if each fasta file \
    contains sequences of single strand. In case any errors, use \
    `-i` to deal with filenames and sequence headers manually."
    s.usage = "./scripts/03_fasta_repack.jl data/assembly/PRJNA1049504/"
    s.version = "1.1.0"
    s.add_version = true

    @add_arg_table! s begin
        "input_dir"
            help = "input directory"
            required = true
            arg_type = String

        "-o", "--output_dir"
            help = "Output directory"
            arg_type = String

        "-i", "--interactive"
            help = "Repacking in interactive mode, automatic otherwise"
            action = :store_true
    end
    return parse_args(s)
end


function check_if_output_dir_repacked(dir_path; interactive=false)
    if ".isrepacked" in readdir(dir_path)
        open(joinpath(dir_path, ".isrepacked")) do rep_flag
            msg = readline(rep_flag)
            if interactive
                @warn "Output directory contains '.isrepacked' flag:\n$msg"
                isrepacked_flag_abort_menu = RadioMenu(["abort", "force continue"])
                isrepacked_flag_abort = request("Should we continue repacking script?", isrepacked_flag_abort_menu)
                (isrepacked_flag_abort == 1) && exit()
            else
                @error "Chosen output directory contains repacked directory flag:"
                @info msg
                exit()
            end
        end
    end
end


function put_repack_flag(path, path_origin)
    open(joinpath(path, ".isrepacked"), "w") do repacked_flag
        write(
            repacked_flag,
            "This directory contains repacked versions of fasta files from $(abspath(path_origin))"
        )
    end
end


function process_directory(dir_in, dir_out)
    file_count = 0
    fasta_files = fasta_names(dir_in)

    if length(Set(strains_in_fasta.(fasta_files))) != length(fasta_files)
        @error "Strains are repeated across fasta files in chosen dir!"
        exit()
    end

    if !isempty(fasta_files)
        mkpath(dir_out)
        check_if_output_dir_repacked(dir_out)
    end

    @showprogress "Processing..." for file in fasta_files
        file_count += process_fasta(file, dir_out)
    end

    return file_count
end


function process_directory_interactive(dir_in, dir_out)
    file_count = 0
    fasta_files = fasta_names(dir_in)
    if isempty(fasta_files)
        println("Provided path contains no fasta files")
        exit()
    end

    if !isempty(fasta_files)
        mkpath(dir_out)
        check_if_output_dir_repacked(dir_out; interactive=true)
    end
    compression_choice_menu = RadioMenu(["leave as is", "force gzip", "force unzip"])
    compression_choice = Dict(
        1=>:do_not_change,
        2=>:gzip_all,
        3=>:unzip_all
    )[request("Do you need compression for repacked files?", compression_choice_menu)]


    for file in fasta_files
        file_count += process_fasta_interactive(file, dir_out; compression=compression_choice)
    end

    return file_count
end

function strains_in_fasta(fasta_file)
    S = Set()
    FastaReader(fasta_file) do fr
        for (header, sequence) in fr
            m = match(r" (?i)strain ([\d\w]+) ", header)
            if !isnothing(m)
                push!(S, m.captures[1])
            else
                @error "Some sequence headers in $fasta_file have no 'strain' information!"
                exit()
            end
        end
    end
    if length(S) > 1
        @error "There are >1 'strain' in $fasta_file"
        exit()
    end
    return replace(pop!(S), " "=>"_")
end

function build_new_header(old_header)
    id = first(split(old_header, " "))
    m = match(r" (?i)strain [\d\w]+ (.*),", old_header)
    id_rest = m.captures[1]
    return "$id $id_rest"
end

function process_fasta(file_path, dir_out)
    strain = strains_in_fasta(file_path)
    fasta_file_suffix = match(MetaFASTA.FASTA_SUFFIX_GZ, file_path).captures[2]
    fasta_out = joinpath(dir_out, strain * fasta_file_suffix)
    n_sequences_wrote = 0
    FastaReader(file_path) do fr
        FastaWriter(fasta_out) do fw
            for (header, sequence) in fr
                new_header = build_new_header(header)
                writeentry(fw, new_header, sequence)
                n_sequences_wrote += 1
            end
        end
        if n_sequences_wrote == 0
            rm(fasta_out)
        end
    end
    return Int(n_sequences_wrote > 0)
end

function process_fasta_interactive(file_path, dir_out; compression=:do_not_change)
    cls()
    fasta_name, fasta_suffix = match(MetaFASTA.FASTA_SUFFIX_GZ, file_path).captures
    file_basename = basename(fasta_name)
    
    if compression == :unzip_all
        fasta_suffix = replace(fasta_suffix, r"\.gz$"=>"")
    elseif compression == :gzip_all
        fasta_suffix = replace(fasta_suffix, r"\.gz$"=>"")
        fasta_suffix *= ".gz"
    end

    # TODO fix typo in README illustration
    println("""
    Interactive repacking with compression option '$compression'
    You are responsible for avoiding name collisions.
    There is no way to undo your choices!
    [Ctrl+C to stop]\n\nnow repacking:\n$file_basename""")
    sequence_options = FastaReader(file_path) do fr
        [bioseq_readlen(sequence) * " " * header for (header, sequence) in fr]
    end
    sequence_menu = MultiSelectMenu(sequence_options)
    sequence_choices = request("Select needed sequences:", sequence_menu)
    
    user_proposal = ""
    while true # user input loop
        println("Enter new filename (no path or suffix!) or press ENTER to use current name:")
        user_proposal = readline()
        user_proposal == "" && break
        occursin(Base.Filesystem.path_separator, user_proposal) && continue
        occursin(MetaFASTA.FASTA_SUFFIX_GZ, user_proposal) && continue
        break
    end
    
    fasta_out = user_proposal == "" ?
        joinpath(dir_out, file_basename * fasta_suffix) :
        joinpath(dir_out, user_proposal * fasta_suffix)

        
    n_sequences_wrote = 0
    FastaReader(file_path) do fr
        FastaWriter(fasta_out) do fw
            menu_counter = 0
            for (header, sequence) in fr
                menu_counter += 1
                !(menu_counter in sequence_choices) && continue
                println("Now for header:\n$header")
                rename_menu = RadioMenu(["it's OK", "edit"])
                rename = request("Change sequence header?", rename_menu)
            
                if (rename==2)
                    while true # user input loop
                        println("Enter new sequence header (it must not contain '>' symbol):")
                        header = readline()
                        if '>' in header
                            continue
                        else
                            break
                        end
                    end
                end
                writeentry(fw, header, sequence)
                n_sequences_wrote += 1
            end
        end
        if n_sequences_wrote == 0
            rm(fasta_out, force=true)
        end
    end
    cls()
    return Int(n_sequences_wrote > 0)
end





function main()
    clargs = parse_commandline()
    input_dir = clargs["input_dir"]
    output_dir = clargs["output_dir"]
    interactive = clargs["interactive"]

    if isnothing(output_dir)
        output_dir = joinpath(input_dir, "repack")
    end

    file_count = interactive ?
        process_directory_interactive(input_dir, output_dir) :
        process_directory(input_dir, output_dir)

    if file_count == 0
        rm(output_dir, recursive=true, force=true)
    else
        put_repack_flag(output_dir, input_dir)
    end
    println("Files processed: $file_count")

end


if abspath(PROGRAM_FILE) == @__FILE__
    try
        main()
    catch e
        if isa(e, InterruptException)
            println("\nCtrl+C caught\nTerminating the script.")
        else
            rethrow(e)
        end
    end
end