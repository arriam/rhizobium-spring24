"""
    bioseq_readlen(seq::AbstractString)::String
    bioseq_readlen(n::Int)::String

Represent string or integer as length of biosequence, measured in bases
with appropriate prefixes: K|M|G. Works up to 999_949_999_999 bp.

# Examples

```jldoctest
julia> bioseq_readlen("AGCT")
"     4 b"

julia> bioseq_readlen(150)
"   150 b"

julia> bioseq_readlen(1_500)
"  1.5 Kb"

julia> bioseq_readlen(150_000)
"150.0 Kb"

julia> bioseq_readlen(15_000_000)
" 15.0 Mb"

julia> bioseq_readlen(1_500_000_000)
"  1.5 Gb"

julia> bioseq_readlen(10^12)
"too long"
```
"""
function bioseq_readlen(n::Int)
    @assert n > 0 "Biosequence length cannot be negative: $n"
    if n < 10
        return lpad(n, 6) * " b"
    elseif n < 100
        return lpad(n, 6) * " b"
    elseif n < 1000
        return lpad(n, 6) * " b"
    elseif n < 1e6
        return lpad(round(n/1e3; digits=1), 5) * " Kb"
    elseif n < 1e9
        return lpad(round(n/1e6; digits=1), 5) * " Mb"
    elseif n < 9.9995e11
        return lpad(round(n/1e9; digits=1), 5) * " Gb"
    end
    return "too long"
end

bioseq_readlen(seq::AbstractString) = bioseq_readlen(length(seq))


"""
    getheader(fh::FastaHeader)
    getheader(fh::FastaSimpleHeader)
    getheader(fm::FastaMetadata)

Reconstruct initial header

# Examples

```jldoctest
julia> header = "CP140886.1 Sinorhizobium medicae strain ml60 chromosome, complete genome";

julia> fh = FastaHeader(header);

julia> fm = FastaMetadata(header, "GCT");

julia> fsh = FastaSimpleHeader(header);

julia> getheader(fh) == getheader(fm) == getheader(fsh) == header
true
```
"""
function getheader(fh::FastaHeader)
    string(
        fh.accession, " ",
        fh.organism, " ",
        "strain $(fh.strain) ",
        fh.molecula, ", ",
        fh.method
    )
end

function getheader(fsh::FastaSimpleHeader)
    string(
        fsh.accession, " ",
        fsh.rest
    )
end

function getheader(fm::FastaMetadata)
    getheader(fm.header)
end


"""
    @name variable

Return variable name as a string
"""
macro name(arg)
    x = string(arg)
    quote
        $x
    end
end