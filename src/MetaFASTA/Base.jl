Base.:(==)(a::FastaMetadata, b::FastaMetadata) = a.len == b.len
Base.isless(a::FastaMetadata, b::FastaMetadata) = a.len < b.len


Base.summary(fm::FastaMetadata) = "FastaMetadata record for $(fm.header.accession): $(bioseq_readlen(fm.len)) sequence."
Base.summary(fh::FastaHeader) = "FastaHeader record for $(fh.accession)."
Base.summary(fsh::FastaSimpleHeader) = "FastaSimpleHeader record for $(fsh.accession)."

function Base.show(io::IO, fh::FastaHeader)
    println(io, "FastaHeader record:")
    println(io, "\tAccession: $(fh.accession)")
    println(io, "\tOrganism: $(fh.organism)")
    println(io, "\tStrain: $(fh.strain)")
    println(io, "\tMolecula: $(fh.molecula)")
    println(io, "\tMethod: $(fh.method)")
end

function Base.show(io::IO, fsh::FastaSimpleHeader)
    println(io, "FastaSimpleHeader record:")
    println(io, "\tAccession: $(fsh.accession)")
    println(io, "\tRest: $(fsh.rest)")
end

function Base.show(io::IO, fm::FastaMetadata{FastaHeader})
    println(io, "FastaMetadata record:")
    println(io, "\tAccession: $(fm.header.accession)")
    println(io, "\tOrganism: $(fm.header.organism)")
    println(io, "\tStrain: $(fm.header.strain)")
    println(io, "\tMolecula: $(fm.header.molecula)")
    println(io, "\tMethod: $(fm.header.method)")
    println(io, "\tLength: ", bioseq_readlen(fm.len))
    println(io, "\tGC: $(fm.gc)")
end

function Base.show(io::IO, fm::FastaMetadata{FastaSimpleHeader})
    println(io, "FastaMetadata record:")
    println(io, "\tAccession: $(fm.header.accession)")
    println(io, "\tRest: $(fm.header.rest)")
    println(io, "\tLength: ", bioseq_readlen(fm.len))
    println(io, "\tGC: $(fm.gc)")
end

Base.print(io::IO, fm::FastaMetadata) = print(io, summary(fm))
Base.println(io::IO, fm::FastaMetadata) = println(io, summary(fm))

Base.print(io::IO, fh::FastaHeader) = print(io, summary(fh))
Base.println(io::IO, fh::FastaHeader) = println(io, summary(fh))

Base.print(io::IO, fsh::FastaSimpleHeader) = print(io, summary(fsh))
Base.println(io::IO, fsh::FastaSimpleHeader) = println(io, summary(fsh))