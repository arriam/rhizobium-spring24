abstract type AbstractFastaHeader end


struct FastaHeader <: AbstractFastaHeader
    accession
    organism
    strain
    molecula
    method
end

"""
    FastaHeader(header::String)

Struct for managing fasta headers.

# Examples

```jldoctest
julia> header = "CP140886.1 Sinorhizobium medicae strain ml60 chromosome, complete genome";

julia> FastaHeader(header)
FastaHeader record:
        Accession: CP140886.1
        Organism: Sinorhizobium medicae
        Strain: ml60
        Molecula: chromosome
        Method: complete genome
```
"""
function FastaHeader(header::String)
    re_accession = r"(^\S+) "
    re_organism = r"^\S+ (.*) (?i)strain"
    re_strain = r"(?i)strain (\S*) "
    re_molecula = r"(?i)strain\S* [\S\d]* (.*),"
    re_method = r", ?(.*)$"

    fields = map([re_accession, re_organism, re_strain, re_molecula, re_method]) do rex
        m = match(rex, header)
        isnothing(m) ? "" : m.captures[1]
    end

    "" in fields && @error "Couldn't parse properly all the fields of \n$header"

    return FastaHeader(fields...)
end


struct FastaSimpleHeader <: AbstractFastaHeader
    accession
    rest
end

"""
    FastaSimpleHeader(header::String)

Struct for managing simplified fasta headers (after repacking).

# Examples

```jldoctest
julia> header = "CP140886.1 Sinorhizobium medicae strain ml60 chromosome, complete genome";

julia> FastaSimpleHeader(header)
FastaSimpleHeader record:
        Accession: CP140886.1
        Rest: Sinorhizobium medicae strain ml60 chromosome, complete genome
```
"""
function FastaSimpleHeader(header::String)
    accession, rest = split(header, ' ', limit=2)
    return FastaSimpleHeader(accession, rest)
end