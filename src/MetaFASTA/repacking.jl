# .fasta
const FASTA_SUFFIX = r"(.+)(\.f(?:n|ast|s)?a)$"

# .fasta | .fasta.gz
const FASTA_SUFFIX_GZ = r"(.+)(\.f(?:n|ast|s)?a(?:\.gz)?)$"

function isrepacked(directory)
    return ".isrepacked" in readdir(directory)
end

"""
    fasta_names(path; style=:all, gzips=true)

Return the names of FASTA files in the given directory.

- `style`: A symbol that determines the style of the returned names:
  - `:all`: Full path to the FASTA files.
  - `:basename`: Returns only the base names of the files.
  - `:noext`: Returns the base names without the file extensions.
- `gzips`: wether to accept `.gz` files.
"""
function fasta_names(path; style=:all, gzips=true)
    @assert style in [:all, :basename, :noext] "Wrong style :$style. Use :all|:basename|:noext"
    fasta_suffix = gzips ? FASTA_SUFFIX_GZ : FASTA_SUFFIX
    
    matches = @chain path begin
        readdir
        match.(fasta_suffix, _)
        filter(!isnothing, _)
        collect.(_)
    end
    
    return begin
        style == :all ? abspath.(path, join.(matches)) :
        style == :basename ? join.(matches) : getindex.(matches, 1)
    end
end


"""
    scrape_fasta(fasta_file; simple_header=false)

This function reads a FASTA file and extracts the metadata, sequences, and stripped filenames.

- `simple_header`: flag indicating whether to use a simple header format (default: `false`).

# Returns
- `metas`: An array of `FastaMetadata` objects containing the metadata for each entry in the FASTA file.
- `sequences`: An array of strings containing the sequences for each entry in the FASTA file.
- `stripped_filenames`: An array of stripped filenames, where each filename is the base name of the input FASTA file without the extension.
"""
function scrape_fasta(fasta_file; simple_header=false)
    metas = FastaMetadata[]
    sequences = String[]
    FastaReader(fasta_file) do fr
        for (header, seq) in fr
            fasta_meta = FastaMetadata(header, seq; simple_header=simple_header)
            push!(metas, fasta_meta)
            push!(sequences, seq)
        end
    end
    stripped_filename = splitext(replace(basename(fasta_file), r"\.gz$"=>""))[1]
    stripped_filenames = fill(stripped_filename, length(metas))
    return metas, sequences, stripped_filenames
end


"""
    scrape_dir(path; repacked_dir=false)

This function reads all FASTA files in the specified directory and extracts their metadata, sequences, and stripped filenames.

- `repacked_dir`: A flag indicating whether the FASTA files have been repacked in the directory (default: `false`).

# Returns
- `dir_fasta_files`: An array of strings containing the paths to the FASTA files in the directory.
- `dir_metas`: An array of `FastaMetadata` objects containing the metadata for each entry in the FASTA files.
- `dir_sequences`: An array of sequences of the FASTA files.
- `dir_stripped_filenames`: An array of stripped filenames, where each filename is the base name of the input FASTA file without the extension.
"""
function scrape_dir(path; repacked_dir=false)
    dir_fasta_files = fasta_names(path; style=:all)
    dir_metas = FastaMetadata[]
    dir_sequences = String[]
    dir_stripped_filenames = String[]

    for fasta_file in dir_fasta_files
        metas, sequences, stripped_filenames = scrape_fasta(fasta_file; simple_header=repacked_dir)
        append!(dir_metas, metas)
        append!(dir_sequences, sequences)
        append!(dir_stripped_filenames, stripped_filenames)
    end
    return dir_fasta_files, dir_metas, dir_sequences, dir_stripped_filenames
end