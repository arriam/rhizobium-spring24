struct FastaMetadata{HeaderType<:AbstractFastaHeader}
    header::HeaderType
    len::Int64
    gc::Float64
end

"""
    FastaMetadata(header::String, seq::String)

Struct for managing metadata for fasta sequence.

# Examples

```jldoctest
julia> header = "CP140886.1 Sinorhizobium medicae strain ml60 chromosome, complete genome";

julia> seq = "TCATCAACGACATTATTTTTTTTCCACAGAGCAGCAGCGAGC";

julia> FastaMetadata(header, seq)
FastaMetadata record:
        Accession: CP140886.1
        Organism: Sinorhizobium medicae
        Strain: ml60
        Molecula: chromosome
        Method: complete genome
        Length: 42  b
        GC: 0.429

julia> FastaMetadata(header, seq; simple_header=true)
FastaMetadata record:
        Accession: CP140886.1
        Rest: Sinorhizobium medicae strain ml60 chromosome, complete genome
        Length: 42  b
        GC: 0.429
```
"""
function FastaMetadata(header::String, seq::String; simple_header=false)
    len = length(seq)
    if len == 0
        @error "Can not parse metadata for empty sequence!"
        exit()
    end
    
    SEQ = uppercase(seq)
    if !occursin('G', SEQ)
        if !occursin('C', SEQ)
            @warn "Sequence has no GC!"
            gc = 0.0
        else
            cm = countmap(SEQ)
            gc = cm['C'] / len
        end
    elseif !occursin('C', SEQ)
        cm = countmap(SEQ)
        gc = cm['G'] / len
    else
        cm = countmap(SEQ)
        gc = (cm['G'] + cm['C']) / len
    end
    
    fheader = simple_header ? FastaSimpleHeader(header) : FastaHeader(header)
    return FastaMetadata(fheader, len, round(gc, digits=3))
end