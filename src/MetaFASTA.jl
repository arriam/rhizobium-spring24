"""
    MetaFASTA

Module for managing metadata of FASTA sequences.
"""
module MetaFASTA

export FastaHeader, FastaSimpleHeader, FastaMetadata
export getheader, bioseq_readlen

export isrepacked, fasta_names, scrape_fasta, scrape_dir

using StatsBase, FastaIO, Chain

include("MetaFASTA/FastaHeader.jl")
include("MetaFASTA/FastaMetadata.jl")
include("MetaFASTA/methods.jl")
include("MetaFASTA/Base.jl")
include("MetaFASTA/repacking.jl")


end #module