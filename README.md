# rhizobium-spring24
Set of pipelines for working with [NCBI](https://www.ncbi.nlm.nih.gov/) bioprojects.

## Table of Contents
<details><summary> Expand </summary>

- [Reproducing the Environment](#reproducing-the-environment)
- [`01_bioprojects_download.jl`](#fetching-genomes-from-ncbi-bioprojects-01_bioprojects_downloadjl): Fetching Genomes from NCBI Bioprojects
- [`02_fasta_stats.jl`](#describe-fasta-files-in-chosen-dir-02_fasta_statsjl): Describe FastA Files in Chosen Dir
- [`03_fasta_repack.jl`](#repack-fasta-files-in-chosen-dir-03_fasta_repackjl): Repack FastA Files in Chosen Dir
- [`04_fasta_cat.jl`](#concatenate-all-sequences-in-file-04_fasta_catjl): Concatenate All Sequences in File
</details>


## Reproducing the environment
<details><summary> Expand </summary>
  
To (locally) reproduce this project, do the following:

0. Install [Julia language](https://julialang.org/downloads/) for your system:

   - For MacOS and Linux execute:
      ```sh
      curl -fsSL https://install.julialang.org | sh
      ```
   - For Windows run:
      ```powershell
      winget install julia -s msstore
      ```
   This package is developed in Linux environment and tested for `Julia 1.10.4`, so there might be unexpected issues if you try run this on other platform or earlier versions of Julia. In any case, you must have `julia` in your PATH.
1. Download this code base. Raw datasets are not included in the repo.
   ```bash
   git clone https://gitlab.com/arriam/rhizobium-spring24.git
   cd rhizobium-spring24
   ```

2. In order to run 1st script you need to have `ncbi-datasets-cli` tool installed in your current environment. Check it with folowing command:
   ```bash
   datasets --version
   ```
   it should return something like
   > datasets version: 16.22.1
   
   To install it painlessly you can use __Conda__ or __Mamba__ and run everything from this environment:
   ```
   conda env create -f environment.yml
   conda activate rhizobium-spring24
   ```
</details>

## Fetch genomes from NCBI bioprojects
<details><summary>Script: 01_bioprojects_download.jl</summary>

> Maka sure to have `ncbi-datasets-cli` availible in your environment as explained above!

Use this script for quick fetching multiple genome assemblies. It downloads, extracts all FastA files into single directory, removes intermediate files.

Get basic help:
```sh
./scripts/01_bioprojects_download.jl --help
```
Example command (~35Mb download, ~36Mb (~120Mb if ran with `--uncompress` flag) after extraction):
```sh
./scripts/01_bioprojects_download.jl \
   --id PRJNA1049504 GCF_004306555.1 \
   --source genbank \ # Option affects only `PRJ*` files, `GC*` files will be downloaded anyway.
   --output_dir data/assembly
```
<p align="center">
<img src="assets/01.png" width="70%" height="auto">
</p>

> By default, the extracted files are still `.gz` compressed (as many tools are able to work with compressed fasta files, which otherwise can occupy quite a lot of disc space). If you want to uncompress extracted files, use `--uncompress` flag.

Each successfully downloaded bioproject creates a separate subdirectory in chosen `output_dir`.
The script tries its best to download as many bioprojects as possible without crashing, but it skips those which directories are not empty.

Currently supports downloading NCBI entries with following prefixes:
* `PRJ` : downloads Bioproject entry. Option `--source` affects only these IDs;
* `GCA_`: downloads GenBank entry;
* `GCF_`: downloads RefSeq entry.
</details>

## Describe FastA files in chosen dir
<details><summary>Script: 02_fasta_stats.jl</summary>

Prints basic info about FastA files into terminal: filename, lengths of sequences, total length of sequences in dir.

Example command (works if you've ran example for `01_bioprojects_download.jl` above):
```sh
./scripts/02_fasta_stats.jl data/assembly/PRJNA1049504 -b
```
<p align="center">
<img src="assets/02.png" width="70%" height="auto">
</p>

where `-b` flag stands for the bar plot. There are other flags, so check the `--help` section as well.
</details>

## Repack FastA files in chosen dir
<details><summary>Script: 03_fasta_repack.jl</summary>

Rename FastA files and sequences inside them in automatic or manual mode.

<details><summary>Automatic mode</summary>

As for now, this mode has limited capabilities and works properly only in case each fasta contains sequences from one `strain`. __Mind that all sequences have to be annotated with `strain`!__

1. Puts repacked fastas into directory, given after `-o` flag of, if none given --
into `repack` subdirectory near input files. It also writes a special flag file `.isrepacked`, which indicates, that all fastas in that dir are repacked, which helps indexing them with `02_fasta_stats` script (do not remove that file!).

2. After repack each fasta is renamed based on strain it contains. Each sequence is renamed as given in examples:

|              |example 1|example 2|
|--------------|----------------------------------------------------------------------|-|
|**Old Filename**|`GCA_035971255.1_ASM3597125v1_genomic.fna`|`GCF_004306555.1_ASM430655v1_genomic.fna`|
|**New Filename**|`SM52.fna`|`TP13.fna`|
|**Old Seq Header**|`NZ_SILH01000001.1 Rhizobium leguminosarum strain SM52 chrom_SM52, whole genome shotgun sequence` |`CP140874.1 Rhizobium beringeri strain TP13 plasmid pRlX1, complete sequence`|
|**New Seq Header**|`NZ_SILH01000001.1 chrom_SM52`|`CP140874.1 plasmid pRlX1`|

   Basicly, it removes strain information (as it becomes filename) and everything after comma.

Example command (works if you've ran example for `01_bioprojects_download.jl` above) :
```sh
./scripts/03_fasta_repack.jl data/assembly/PRJNA1049504/
```
After repack finishes feel free to run:
```sh
./scripts/02_fasta_stats.jl data/assembly/PRJNA1049504/ -b
```
</details>

> If you are not satisfied with the result of automatic repack or script throws errors (which means, sequence headers didn't match some pre-defined regular expressions), you may use interactive mode:

<details><summary>Interactive mode</summary>

With `-i` flag user will be prompted to choose output filename(s), keep or throw away any sequences in fasta files, use __gzip__ compression or, on the other hand, __gunzip__ all the files. Example usecase:
```sh
./scripts/03_fasta_repack.jl data/assembly/PRJNA1049504/ -i
```
<p align="center">
<img src="assets/03.png" width="70%" height="auto">
</p>

To exit running script prematurely feel free to hit `Ctrl+C`.
</details>
</details>

## Concatenate all sequences in file
<details><summary>Script: 04_fasta_cat.jl</summary>

Writes all sequences of fasta file into new file with `_cat` postfix as a single sequence. All the headers are concatenated as well with the addition of sequence lengths as follows:
original:
```txt
>header 1
AAAAAAA
>header_2
BBBB
>header3
CCCCCCCCC
```
concatenated (each sequence length surrounded by `#` symbols):
```txt
>#7#header 1#4#header_2#9#header3
AAAAAAABBBBCCCCCCCCC
```
If used with `-s` flag, sequences are reordered by descending of their lengths:
```txt
>#9#header3#7#header 1#4#header_2
CCCCCCCCCAAAAAAABBBB
```
By default, concatenates are being put into `cats/` subdir near the original files, but you can give that dir a different name using `-o` flag, or chose to put the resulting files in the same dir as the originals: `-o '.'`

Example command (you can use give multiple filenames at once or use wildcard -- only `.fna`, `.fsa`, `.fa`, `.fasta` files will be processed):
```sh
./scripts/04_fasta_cat.jl -s data/assembly/PRJNA1049504/*
```

<p align="center">
<img src="assets/04.png" width="70%" height="auto">
</p>
</details>