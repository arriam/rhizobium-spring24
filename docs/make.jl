using DrWatson
@quickactivate

include(srcdir("MetaFASTA.jl"))
# push!(LOAD_PATH,"../src/")
using Documenter, .MetaFASTA

makedocs(
    sitename="Rhizobium spring`24",
    # modules = [MetaFASTA],
    authors = "Phlaster",
    remotes=nothing,
    pages = [
        "Home" => "index.md",
        "FastaHeaders" => "fastaheaders.md",
        "FastaMetadata" => "fastametadata.md",
        "Other functions" => "methods.md"
    ]
)