# Functions

## getheader
```@docs
getheader(fh::FastaHeader)
```

## bioseq_readlen
```@docs
bioseq_readlen(n::Int; digits=2)
```