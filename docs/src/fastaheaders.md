# Fasta headers

## FastaHeader
```@docs
FastaHeader(header::String)
```

## FastaSimpleHeader
```@docs
FastaSimpleHeader(header::String)
```
